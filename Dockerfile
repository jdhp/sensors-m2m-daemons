#FROM python:3
FROM python:3.8-slim

WORKDIR /app


# UPDATE PIP ##################################################################

RUN pip install --no-cache-dir --upgrade pip


# INSTALL THE PROJECT #########################################################

RUN pip install --no-cache-dir "m2m"