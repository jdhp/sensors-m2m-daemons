===========
M2M Daemons
===========

M2M Daemons

Note:

    This project is in beta stage, the API may change.

Contents:

.. toctree::
   :maxdepth: 2

   intro
   API <api>
   developer

* Web site: http://www.jdhp.org/software_en.html#m2m
* Online documentation: https://jdhp.gitlab.io/m2m
* Source code: https://gitlab.com/jdhp/sensors-m2m-daemons
* Issue tracker: https://gitlab.com/jdhp/sensors-m2m-daemons/issues
* m2m on PyPI: https://pypi.org/project/m2m

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Credits
=======

.. include:: ../AUTHORS
   :literal:

License
=======

.. highlight:: none

.. include:: ../LICENSE
   :literal:

